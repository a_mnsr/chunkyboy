#include <stdio.h>
#include <stdlib.h>
#include "user.h"
typedef struct
{
    STACK_DATA_TYPE arr[SIZE];
    unsigned short top ;
}tstack;

void STK_Create(tstack* ps);
void STK_Push(tstack* ps, STACK_DATA_TYPE e);
void STK_POP(tstack* ps, STACK_DATA_TYPE *pe);
int STK_Full(tstack* ps);
int STK_Empty(tstack* ps);
void STK_top (tstack* ps, STACK_DATA_TYPE*pe);
int STK_Size(tstack* ps);
void STK_Clear(tstack* ps);

