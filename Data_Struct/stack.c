
#include <stdio.h>
#include <stdlib.h>
#include "user.h"
#include "stack.h"

void STK_Create(tstack* ps)
{
    ps->top =0;

}
void STK_Push(tstack* ps, STACK_DATA_TYPE e)
{
    ps->arr[ps->top++]=e;
}

void STK_POP(tstack* ps, STACK_DATA_TYPE *pe)
{
   *pe=ps->arr[--(ps->top)];
}
int STK_Full(tstack* ps)
{
    if(ps->top==SIZE)
        return 1;
    return 0;
}
int STK_Empty(tstack* ps)
{
        if(ps->top==0)
        return 1;
    return 0;
}

void STK_top (tstack* ps, STACK_DATA_TYPE*pe)
{

    *pe=ps->arr[(ps->top)-1];

}
int STK_Size(tstack* ps)
{
    return ps->top;
}

void STK_Clear(tstack* ps)
{
    ps->top=0;
}







